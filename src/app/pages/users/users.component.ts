import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { Fields, OptionSelect } from 'src/app/shared/components/dinamic-form/interfaces/fields';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { IconsService } from 'src/app/shared/services/icons/icons.service';
import { RoleService } from 'src/app/shared/services/roles/role.service';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';
import { User } from '../accounts/interfaces/account';


@Component({
  selector: 'mvd-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'ID', col: 'id' },
    { title: 'Foto', col: 'image' },
    { title: 'Nombre', col: 'name' },
    { title: 'Email', col: 'email' },

  ]

  dataSource: Observable<any[]>;

  ////paginator
  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroptions: Subscription[] = [];
  itemEdit: User;
  fields: Fields[]
  optionsRoles: OptionSelect[];

  /////////////


  constructor(
    private usersService: UserService,
    public dialog: MatDialog,
    private roleService: RoleService,
    private router: Router
  ) {
    this.result = this.usersService.resultItems$

  }


  ngOnInit(): void {
    this.getUsers(this.pageDefault, this.perPage, this.filter, this.orden)
    this.subscroptions.push( this.roleService.getRoles().subscribe(
      res=> {
        this.optionsRoles = res.map(v=> {return {name : v.name, value: v.id}})
      }
    ) )

  }

  paginatorChange(e: PageEvent) {
    console.log(e);
    this.getUsers(e.pageIndex + 1, e.pageSize)

  }



  getUsers(currentPage?, perPage?, filter?, sort?) {
    this.subscroptions.push(this.usersService.getUsers(currentPage, perPage, filter, sort).subscribe(next => this.loadData())) ;
  }

  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: `${x.name} ${x.last_name}`,
          email: x.email,
          image: x.account.image

        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getUsers(this.pageDefault, this.perPage, filter, this.orden)
  }

  deleteItem(id): Observable<any> {
    return this.usersService.deleteUser(id)
  }

  itemAction(event) {
    console.log(event);

    console.log(event);

    if(event.action === "delete"){
      this.deleteItem(event.element.id).pipe(take(1)).subscribe( res=> console.log(res))
    }

    if(event.action === "edit"){
      this.router.navigate(['/usuarios/editar-usuario', event.element.id])
    }


  }



  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscroptions.map(v=> v.unsubscribe())
  }

  add(){

  }

}
