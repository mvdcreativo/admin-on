import { Injectable, OnDestroy } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../auth.service';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private activateRouter: ActivatedRoute
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {



      this.authService.checkUser()


      const user = this.authService.currentUser.value

      return this.check(next,user)

  }


  check(next, user):boolean {

    if (+next.params.id === +user?.id) {
      return true;
    }else{
      console.log("No Autorizado");

      this.router.navigate(['/auth/login']);
      return false

    }
  }
}
